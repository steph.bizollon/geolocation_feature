<?php

namespace Tests\AppBundle\Geolocation\Client;

use AppBundle\Exception\PartnerException;
use AppBundle\Geolocation\Client\GooglePlacesClient;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class GooglePlacesClientTest extends TestCase
{
    private $mockClient;

    public function setUp()
    {
        $this->mockClient = $this->createMock(Client::class);
    }

    private function initMockService(array $methods = null): MockObject
    {
        $mockService = $this->getMockBuilder(GooglePlacesClient::class)
            ->setMethods($methods)
            ->disableOriginalConstructor()
            ->getMock();

        $reflectionClass = new \ReflectionClass(GooglePlacesClient::class);
        $reflectionProperty = $reflectionClass->getProperty('client');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($mockService, $this->mockClient);

        return $mockService;
    }

    public function testConstructor()
    {
        $mockContainer = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods(['hasParameter', 'getParameter'])
            ->getMockForAbstractClass();

        $mockContainer->expects($this->once())
            ->method('hasParameter')
            ->with('geolocation')
            ->willReturn(true);

        $mockContainer->expects($this->once())
            ->method('getParameter')
            ->with('geolocation')
            ->willReturn([
                'google_places_host' => 'host',
                'google_places_key' => 'key',
            ]);

        $this->getMockBuilder(GooglePlacesClient::class)
            ->setConstructorArgs([
                $mockContainer,
            ])
            ->getMock();
    }

    public function testGetPlaceWillReturnPartnerException()
    {
        $term = 'Paris+France';
        $mockService = $this->initMockService(['sendRequest']);
        $mockResponse = $this->createMock(Response::class);

        $this->mockClient->expects($this->once())
            ->method('request')
            ->willReturn($mockResponse);

        $mockBody = $this->createMock(StreamInterface::class);

        $mockResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($mockBody);

        $mockBody->expects($this->once())
            ->method('getContents')
            ->willReturn('{"status": "ERROR", "error_message": "google error message"}');

        $this->expectException(PartnerException::class);

        $placeId = $mockService->getPlace($term);
    }

    public function testGetPlaceWillReturnPlaceId()
    {
        $term = 'Paris+France';
        $mockService = $this->initMockService(['sendRequest']);
        $mockResponse = $this->createMock(Response::class);

        $this->mockClient->expects($this->once())
            ->method('request')
            ->willReturn($mockResponse);

        $mockBody = $this->createMock(StreamInterface::class);

        $mockResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($mockBody);

        $mockBody->expects($this->once())
            ->method('getContents')
            ->willReturn('{"candidates":[{"place_id":"place_id_value"}]}');

        $placeId = $mockService->getPlace($term);
        $this->assertEquals('place_id_value', $placeId);
    }

    public function testGetPlaceDetailWillReturnApiResponse()
    {
        $placeId = 'Place ID';
        $mockService = $this->initMockService(['sendRequest']);
        $mockResponse = $this->createMock(Response::class);

        $this->mockClient->expects($this->once())
            ->method('request')
            ->willReturn($mockResponse);

        $mockBody = $this->createMock(StreamInterface::class);

        $mockResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($mockBody);

        $mockBody->expects($this->once())
            ->method('getContents')
            ->willReturn('{"candidates":[{"id":"1", "city":"Paris"}]}');

        $apiResponse = $mockService->getPlaceDetail($placeId);
        $this->assertNotEmpty($apiResponse);
    }
}
