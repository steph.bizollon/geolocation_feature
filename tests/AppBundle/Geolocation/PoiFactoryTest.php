<?php

namespace Tests\AppBundle\Geolocation;

use AppBundle\Entity\Db\Geolocation\Poi;
use AppBundle\Exception\FactoryException;
use AppBundle\Geolocation\Client\PlacesClientInterface;
use AppBundle\Geolocation\PoiFactory;
use AppBundle\Geolocation\Provider\PlacesProviderInterface;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class PoiFactoryTest extends TestCase
{
    private $mockHmEm;

    private $mockPoiRepository;

    private $mockPlacesClient;

    private $mockPlacesProvider;

    public function setUp()
    {
        $this->mockHmEm = $this->getMockBuilder(EntityManager::class)
            ->setMethods(['getRepository'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPlacesClient = $this->createMock(PlacesClientInterface::class);
        $this->mockPlacesProvider = $this->createMock(PlacesProviderInterface::class);
    }

    private function initMockService(array $methods = null): MockObject
    {
        $this->mockPoiRepository = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['findOneBy'])
            ->getMock();
        $mockService = $this->getMockBuilder(PoiFactory::class)
            ->setMethods($methods)
            ->setConstructorArgs([
                $this->mockHmEm,
                $this->mockPlacesClient,
                $this->mockPlacesProvider,
            ])
            ->getMock();
        $reflectionClass = new \ReflectionClass(PoiFactory::class);
        $poiRepositoryProperty = $reflectionClass->getProperty('poiRepository');
        $poiRepositoryProperty->setAccessible(true);
        $poiRepositoryProperty->setValue($mockService, $this->mockPoiRepository);

        return $mockService;
    }

    public function testConstructor()
    {
        $this->mockHmEm->expects($this->once())
            ->method('getRepository')
            ->with(Poi::class)
            ->willReturn($this->mockPoiRepository);

        $mockService = $this->getMockBuilder(PoiFactory::class)
            ->setConstructorArgs([
                $this->mockHmEm,
                $this->mockPlacesClient,
                $this->mockPlacesProvider,
            ])
            ->getMock();

        $reflectionClass = new \ReflectionClass(PoiFactory::class);
        $poiRepositoryProperty = $reflectionClass->getProperty('poiRepository');
        $poiRepositoryProperty->setAccessible(true);
        $poiRepository = $poiRepositoryProperty->getValue($mockService);

        $placesClientProperty = $reflectionClass->getProperty('placesClient');
        $placesClientProperty->setAccessible(true);
        $placesClient = $placesClientProperty->getValue($mockService);

        $placesProviderProperty = $reflectionClass->getProperty('placesProvider');
        $placesProviderProperty->setAccessible(true);
        $placesProvider = $placesProviderProperty->getValue($mockService);

        $this->assertSame($this->mockPoiRepository, $poiRepository);
        $this->assertSame($this->mockPlacesClient, $placesClient);
        $this->assertSame($this->mockPlacesProvider, $placesProvider);
    }

    public function testGetPoiThrowFactoryException()
    {
        $placeToSearch = '1 rue de Paris, France';
        $mockService = $this->initMockService();

        $this->mockPlacesClient->expects($this->once())
            ->method('getPlace')
            ->with($placeToSearch)
            ->willThrowException(new FactoryException());

        $this->expectException(FactoryException::class);

        $mockService->getPoi($placeToSearch);
    }

    public function testGetPoi()
    {
        $placeToSearch = '1 rue de Paris, France';
        $mockService = $this->initMockService();

        $mockPoi = $this->createMock(Poi::class);

        $this->mockPlacesClient->expects($this->once())
            ->method('getPlace')
            ->with($placeToSearch)
            ->willReturn('place_id');

        $this->mockPoiRepository->expects($this->once())
            ->method('findOneBy')
            ->with([
                'googlePlaceId' => 'place_id',
            ])
            ->willReturn($mockPoi);

        $this->mockPlacesClient->expects($this->once())
            ->method('getPlaceDetail')
            ->with('place_id')
            ->willReturn(['data']);

        $this->mockPlacesProvider->expects($this->once())
            ->method('hydratePoi')
            ->with($mockPoi, ['data'])
            ->willReturn($mockPoi);

        $poi = $mockService->getPoi($placeToSearch);
        $this->assertSame($mockPoi, $poi);
    }
}
