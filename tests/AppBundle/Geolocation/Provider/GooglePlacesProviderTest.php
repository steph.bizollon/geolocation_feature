<?php

namespace Tests\AppBundle\Geolocation\Provider;

use AppBundle\Entity\Db\Geolocation\Poi;
use AppBundle\Geolocation\Provider\GooglePlacesProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class GooglePlacesProviderTest extends TestCase
{
    private function initMockService(array $methods = null): MockObject
    {
        return $this->getMockBuilder(GooglePlacesProvider::class)
            ->setMethods($methods)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testHydratePoi()
    {
        $data = [
            'result' => [
                'address_components' => [
                    [
                        'long_name' => 1,
                        'types' => ['street_number'],
                    ],
                    [
                        'long_name' => 'rue de Paris',
                        'types' => ['route'],
                    ],
                    [
                        'long_name' => 75000,
                        'types' => ['postal_code'],
                    ],
                    [
                        'long_name' => 'Paris',
                        'types' => ['locality'],
                    ],
                    [
                        'long_name' => 'France',
                        'types' => ['country'],
                    ],
                ],
                'geometry' => [
                    'location' => [
                        'lat' => 1,
                        'lng' => 2,
                    ],
                ],
                'name' => 'name_value',
                'place_id' => 'place_id_value',
                'permanently_closed' => true,
                'opening_hours' => ['opening_hours_value'],
                'photos' => ['photos_value'],
                'formatted_phone_number' => '0123456789',
            ],
        ];
        $mockPoi = $this->createMock(Poi::class);
        $mockService = $this->initMockService(['hydrateAddress']);

        $mockPoi->expects($this->once())
            ->method('setPostalCode')
            ->with(75000);

        $mockPoi->expects($this->once())
            ->method('setCity')
            ->with('Paris');

        $mockPoi->expects($this->once())
            ->method('setCountry')
            ->with('France');

        $mockPoi->expects($this->once())
            ->method('setAddress')
            ->with('1 rue de Paris');

        $mockPoi->expects($this->once())
            ->method('setName')
            ->with($data['result']['name']);

        $mockPoi->expects($this->once())
            ->method('setLatitude')
            ->with(1);

        $mockPoi->expects($this->once())
            ->method('setLongitude')
            ->with(2);

        $mockPoi->expects($this->once())
            ->method('setGooglePlaceId')
            ->with('place_id_value');

        $mockPoi->expects($this->once())
            ->method('setPermanentlyClosed')
            ->with(true);

        $mockPoi->expects($this->once())
            ->method('setOpeningHours')
            ->with(['opening_hours_value']);

        $mockPoi->expects($this->once())
            ->method('setPhotoUrl')
            ->with($data['result']['photos']);

        $mockPoi->expects($this->once())
            ->method('setPhoneNumber')
            ->with('0123456789');

        $mockService->hydratePoi($mockPoi, $data);
    }
}
