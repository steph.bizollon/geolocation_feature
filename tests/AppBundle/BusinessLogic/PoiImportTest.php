<?php

namespace Tests\AppBundle\BusinessLogic;

use AppBundle\BusinessLogic\PoiImport as PoiImportService;
use AppBundle\Entity\Db\Geolocation\PoiAwaitingConfirmation;
use AppBundle\Entity\Db\Geolocation\PoiImport;
use AppBundle\Exception\PoiImportException;
use AppBundle\Geolocation\PoiFactory;
use AppBundle\Repository\Db\Geolocation\PoiAwaitingConfirmationRepository;
use AppBundle\Repository\Db\Geolocation\PoiImportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Path\To\User;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class PoiImportTest extends TestCase
{
    private $poiImportService;
    protected $em;
    private $poiImportRepository;
    private $poiAwaitingConfirmationRepository;
    private $geolocpath;

    public function setUp()
    {
        $poiFactory = $this->getMockBuilder(PoiFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->poiImportRepository = $this->getMockBuilder(PoiImportRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->poiAwaitingConfirmationRepository = $this->getMockBuilder(PoiAwaitingConfirmationRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $em = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturn($this->poiImportRepository);
        $em->expects($this->any())
            ->method('flush')
            ->willReturn(null);

        $this->geolocpath = 'path';

        $this->poiImportService = new PoiImportService($em, $poiFactory, $this->geolocpath);
    }

    public function testGetPoiImportWithWaitingStatusReturnsNullWithoutData()
    {
        $call = $this->poiImportService->getPoiImportWithWaitingStatus();

        $this->assertEquals(null, $call);

        $object1 = new PoiImport();
        $object2 = new PoiImport();
        $object1->setId(1);
        $object2->setId(2);
        $collection = [$object1, $object2];

        $this->poiImportRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($collection);

        $call2 = $this->poiImportService->getPoiImportWithWaitingStatus();

        $this->assertSame($collection, $call2);
    }

    public function testProcessImportReturnsErrorMessageInArrayWithEmptyFilename()
    {
        $object1 = new PoiImport();
        $object1->setId(1);
        $object1->setFilename('');
        $collection = [$object1];

        $this->assertSame(
            ['An error has occured during poi import id 1 : The csv file name is empty !', null],
            $this->poiImportService->processImport($collection)
        );
    }

    public function testProcessImportReturnsErrorMessageInArrayWithIncompleteFilenames()
    {
        $object1 = new PoiImport();
        $object1->setId(1);
        $object1->setFilename('test');
        $collection = [$object1];

        $this->assertSame(
            ["An error has occured during poi import id 1 : The given csv file doesn't exist : test", null],
            $this->poiImportService->processImport($collection)
        );
    }

    public function testBuildSearchString()
    {
        $poiAwaitingConfirmation = $this->getMockBuilder(PoiAwaitingConfirmation::class)->getMock();
        $poiAwaitingConfirmation->method('getName')->willReturn('test');
        $poiAwaitingConfirmation->method('getCity')->willReturn('testVille');

        $this->assertSame('test+testVille+', $this->poiImportService->buildSearchString($poiAwaitingConfirmation));

        $poiAwaitingConfirmation2 = $this->getMockBuilder(PoiAwaitingConfirmation::class)->getMock();

        $this->expectException(PoiImportException::class);

        $this->poiImportService->buildSearchString($poiAwaitingConfirmation2);
    }

    public function testcleanOutdatedPoiAwaitingConfirmationUpdateOutdatedPoiImportStatus()
    {
        $userMock = $this->getMockBuilder(Utilisateur::class)
            ->disableOriginalConstructor()
            ->getMock();

        $outdatedPoiImport = new PoiImport();
        $outdatedPoiImport->setId(1);
        $outdatedPoiImport->setFilename('test.csv');
        $outdatedPoiImport->setCreatedAt(new \DateTime('2012-12-21 15:00:00'));
        $outdatedPoiImport->setStatus(PoiImport::STATUS_READY);
        $outdatedPoiImport->setCreatedBy($userMock);

        $validPoiImport = new PoiImport();
        $validPoiImport->setId(2);
        $validPoiImport->setFilename('test.csv');
        $validPoiImport->setCreatedAt(new \DateTime());
        $validPoiImport->setStatus(PoiImport::STATUS_READY);
        $validPoiImport->setCreatedBy($userMock);

        $poiImportList = [$outdatedPoiImport, $validPoiImport];

        $outdatedPoiAwaitingConfirmation = new PoiAwaitingConfirmation();
        $outdatedPoiAwaitingConfirmation->setId(1);
        $outdatedPoiAwaitingConfirmation->setName('outdatedName');
        $outdatedPoiAwaitingConfirmation->setPoiImport($outdatedPoiImport);

        $validPoiAwaitingConfirmation = new PoiAwaitingConfirmation();
        $validPoiAwaitingConfirmation->setId(1);
        $validPoiAwaitingConfirmation->setName('validName');
        $validPoiAwaitingConfirmation->setPoiImport($validPoiImport);

        $poiAwaitingConfirmationList = [$outdatedPoiAwaitingConfirmation, $validPoiAwaitingConfirmation];

        $this->poiImportRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($poiImportList);

        $this->poiAwaitingConfirmationRepository->expects($this->any())
            ->method('findAll')
            ->willReturn($poiAwaitingConfirmationList);

        $this->poiImportService->AutoCancelOutdatedPoiImport();

        $this->assertSame(PoiImport::STATUS_CANCELED, $outdatedPoiImport->getStatus());
        $this->assertSame(PoiImport::STATUS_READY, $validPoiImport->getStatus());
    }
}
