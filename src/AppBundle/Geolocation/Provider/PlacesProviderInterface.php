<?php

namespace AppBundle\Geolocation\Provider;

use AppBundle\Entity\Db\Geolocation\Poi;

interface PlacesProviderInterface
{
    public function hydratePoi(Poi $poi, array $data): Poi;
}
