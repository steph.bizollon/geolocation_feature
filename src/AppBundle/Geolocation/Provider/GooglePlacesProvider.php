<?php

namespace AppBundle\Geolocation\Provider;

use AppBundle\Entity\Db\Geolocation\Poi;
use AppBundle\Exception\PartnerException;

class GooglePlacesProvider implements PlacesProviderInterface
{
    public function hydratePoi(Poi $poi, array $data): Poi
    {
        if (!isset($data['result']) || !is_array($data['result'])) {
            return $poi;
        }
        $result = $data['result'];
        if (isset($result['address_components']) && is_array($result['address_components']) && !empty($result['address_components'])) {
            $this->hydrateAddress($poi, $result['address_components']);
        }
        if (isset($result['name'])) {
            $poi->setName($result['name']);
        }
        if (isset($result['geometry']['location']['lat'], $result['geometry']['location']['lng'])) {
            $poi->setLatitude($result['geometry']['location']['lat']);
            $poi->setLongitude($result['geometry']['location']['lng']);
        }
        if (isset($result['place_id'])) {
            $poi->setGooglePlaceId($result['place_id']);
        }
        if (isset($result['permanently_closed'])) {
            $poi->setPermanentlyClosed($result['permanently_closed']);
        }
        if (isset($result['opening_hours'])) {
            $poi->setOpeningHours($result['opening_hours']);
        }
        if (isset($result['photos']) && is_array($result['photos'])) {
            $poi->setPhotoUrl($result['photos']);
        }
        if (isset($result['formatted_phone_number'])) {
            $poi->setPhoneNumber($result['formatted_phone_number']);
        }

        return $poi;
    }

    private function hydrateAddress(Poi $poi, array $data)
    {
        $address = '';
        foreach ($data as $addressComponent) {
            if (!isset($addressComponent['long_name']) || !isset($addressComponent['types'])) {
                continue;
            }
            if (in_array('street_number', $addressComponent['types'])) {
                $address = $addressComponent['long_name'] . ' ' . $address;
            }
            if (in_array('route', $addressComponent['types'])) {
                $address = $address . $addressComponent['long_name'];
            }
            if (in_array('postal_code', $addressComponent['types'])) {
                $poi->setPostalCode($addressComponent['long_name']);
            }
            if (in_array('locality', $addressComponent['types'])) {
                $poi->setCity($addressComponent['long_name']);
            }
            if (in_array('country', $addressComponent['types'])) {
                $poi->setCountry($addressComponent['long_name']);
            }
        }

        if (!empty($address)) {
            $poi->setAddress(trim($address));
        } else {
            throw new PartnerException('No address in response');
        }
    }
}
