<?php

namespace AppBundle\Geolocation\Client;

interface PlacesClientInterface
{
    public function getPlace(string $term): ?string;

    public function getPlaceDetail(string $placeId): array;
}
