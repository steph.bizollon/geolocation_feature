<?php

namespace AppBundle\Geolocation\Client;

use AppBundle\Exception\PartnerException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Container\ContainerInterface;

class GooglePlacesClient implements PlacesClientInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $host;

    public function __construct(ContainerInterface $container)
    {
        if (!$container->hasParameter('geolocation')) {
            throw new \Exception('Parameter missing : geolocation');
        }
        $geolocation = $container->getParameter('geolocation');
        if (!isset($geolocation['google_places_host']) || !isset($geolocation['google_places_key'])) {
            throw new \Exception('Geolocation parameter missing : google_places_uri or google_places_key');
        }
        $this->client = new Client();
        $this->host = $geolocation['google_places_host'];
        $this->apiKey = $geolocation['google_places_key'];
    }

    public function getPlace(string $term): ?string
    {
        $params = '?input=' . $term . '' . '&inputtype=textquery&fields=place_id,name,formatted_address,permanently_closed,opening_hours,geometry/location';
        $arrayResponse = $this->sendRequest('/maps/api/place/findplacefromtext/json', $params);

        if (
            !isset($arrayResponse['candidates'])
            || !is_array($arrayResponse['candidates'])
            || empty($arrayResponse['candidates'])
        ) {
            if (isset($arrayResponse['error_message'])) {
                throw new PartnerException($arrayResponse['status'].' : '.$arrayResponse['error_message']);
            }
            throw new PartnerException($arrayResponse['status']);
        }
        $placeId = null;
        foreach ($arrayResponse['candidates'] as $candidate) {
            if (isset($candidate['place_id'])) {
                $placeId = $candidate['place_id'];
                break;
            }
        }

        return $placeId;
    }

    public function getPlaceDetail(string $placeId): array
    {
        $params = '?place_id=' . $placeId . '&fields=address_component,formatted_address,formatted_phone_number,geometry/location,name,permanently_closed,photo,place_id,plus_code,type,utc_offset';

        return $this->sendRequest('/maps/api/place/details/json', $params);
    }

    private function sendRequest(string $path, string $params): array
    {
        $uri = $this->host . $path . $params . '&key=' . $this->apiKey;
        try {
            $response = $this->client->request('GET', $uri);
            $content = $response->getBody()->getContents();
            $res = json_decode($content, true);
        } catch (RequestException $e) {
            throw new PartnerException('Error while sending request : ' . $e->getMessage());
        }

        return $res ? $res : [];
    }
}
