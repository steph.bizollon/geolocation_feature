<?php

namespace AppBundle\Geolocation;

use AppBundle\Entity\Hm\Geolocation\Poi;
use AppBundle\Exception\FactoryException;
use AppBundle\Exception\PartnerException;
use AppBundle\Geolocation\Client\PlacesClientInterface;
use AppBundle\Geolocation\Provider\PlacesProviderInterface;
use AppBundle\Repository\Db\Geolocation\PoiRepository;
use Doctrine\ORM\EntityManagerInterface;

class PoiFactory
{
    /**
     * @var PoiRepository
     */
    private $poiRepository;

    /**
     * @var PlacesClientInterface
     */
    private $placesClient;

    /**
     * @var PlacesProviderInterface
     */
    private $placesProvider;

    public function __construct(EntityManagerInterface $hmEm, PlacesClientInterface $placesClient, PlacesProviderInterface $placesProvider)
    {
        $this->placesClient = $placesClient;
        $this->placesProvider = $placesProvider;
        $this->poiRepository = $hmEm->getRepository(Poi::class);
    }

    /**
     * @param  string           $place the research terms without space (ie: 'macdo+111+Cours+de+Vincennes+75020+Paris')
     * @return Poi
     * @throws FactoryException
     */
    public function getPoi(string $place): Poi
    {
        try {
            $placeId = $this->placesClient->getPlace($place);
        } catch (PartnerException $e) {
            throw new FactoryException('Cannot get poi for "'.$place.'", Google Places exception : '.$e->getMessage().'.');
        }

        if ($placeId) {
            $poi = $this->poiRepository->findOneBy([
                'googlePlaceId' => $placeId,
            ]);
            if (!$poi) {
                $poi = new Poi();
            }
            try {
                $poiData = $this->placesClient->getPlaceDetail($placeId);

                return $this->placesProvider->hydratePoi($poi, $poiData);
            } catch (PartnerException $e) {
                throw new FactoryException('Cannot get poi for "'.$place.'", Google Places exception : '.$e->getMessage().'.');
            }
        }

        throw new FactoryException('Cannot get poi, Google Places service is down or "' . $place . '" place not found.');
    }
}
