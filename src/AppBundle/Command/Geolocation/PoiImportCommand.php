<?php

namespace AppBundle\Command\Geolocation;

use AppBundle\BusinessLogic\PoiImport;
use AppBundle\Exception\PoiImportException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PoiImportCommand extends ContainerAwareCommand
{
    /**
     * @var string the command name
     */
    protected static $defaultName = 'cron:geolocation:prepare-poi-import';

    protected function configure()
    {
        $this->setName(self::$defaultName)
            ->setDescription('Prepare the geolocation POI list to import for back-office confirmation.')
            ->setHelp('Command for a CRON. Nothing to do.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $poiImportService = $this->getContainer()->get(PoiImport::class);
        $fullLogs = [];
        try {
            $fullLogs[] = $poiImportService->AutoCancelOutdatedPoiImport();
            $poiImportList = $poiImportService->getPoiImportWithWaitingStatus();

            if (!empty($poiImportList)) {
                $fullLogs = $poiImportService->processImport($poiImportList);
            } else {
                $output->writeln('Nothing to import, all poi_imports status are "READY", "CANCELED", "IMPORTED" '
                    .'or poi_import table is empty.');
            }
        } catch (PoiImportException $e) {
            $output->writeln($e->getMessage());
        }

        foreach ($fullLogs as $poiImportLogs) {
            if (!empty($poiImportLogs)) {
                foreach ($poiImportLogs as $log) {
                    if (!empty($log)) {
                        $output->writeln($log);
                    }
                }
            }
        }
        $output->writeln('EXECOK');

        exit();
    }
}
