<?php

namespace AppBundle\BusinessLogic;

use AppBundle\Entity\Db\Geolocation\PoiAwaitingConfirmation;
use AppBundle\Entity\Db\Geolocation\PoiImport as PoiImportEntity;
use AppBundle\Exception\FactoryException;
use AppBundle\Exception\PoiImportException;
use AppBundle\Geolocation\PoiFactory;
use Doctrine\ORM\EntityManagerInterface;

class PoiImport
{
    const CSV_SEPARATOR = ';';

    /**
     * @var PoiFactory
     */
    private $poiFactory;
    /**
     * @var array
     */
    private $csvParsingOptions;
    /**
     * @var array
     */
    private $poiAwaitingConfirmationError;

    public function __construct(EntityManagerInterface $em, PoiFactory $poiFactory, string $geolocPath)
    {
        $this->em = $em;
        $this->poiFactory = $poiFactory;
        $this->csvParsingOptions = [
            'directory' => $geolocPath,
            'filename' => '',
            'ignoreFirstLine' => true,
        ];
    }

    public function getPoiImportWithWaitingStatus(): ?array
    {
        $poiImportRepo = $this->em->getRepository(PoiImportEntity::class);
        $poiImportList = $poiImportRepo->findBy(['status' => PoiImportEntity::STATUS_WAITING]);

        if (!empty($poiImportList)) {
            return $poiImportList;
        }

        return null;
    }

    /**
     * Process given PoiImport entities in order to fill PoiAwaitingConfirmation and Poi tables from csv files extracted
     * addresses and Google api search responses.
     *
     * @param  PoiImportEntity[] $poiImportList
     * @return array             all error messages encountered during this process
     */
    public function processImport(array $poiImportList): array
    {
        $error = [];

        foreach ($poiImportList as $poiImport) {
            $this->updatePoiImportStatus($poiImport, PoiImportEntity::STATUS_READY);
            try {
                $filename = $poiImport->getFilename();
                if (empty($filename)) {
                    throw new PoiImportException('The csv file name is empty !');
                }
                $poiData = $this->getData($filename);
                $error[] = $this->processPoiAwaitingConfirmationImports($poiData, $poiImport);
            } catch (PoiImportException $e) {
                $error[] = 'An error has occured during poi import id '.$poiImport->getId().' : '.$e->getMessage();
            }
        }

        $error[] = $this->poiAwaitingConfirmationError;

        return $error;
    }

    private function getData(string $filename): array
    {
        $poiData = [];
        $this->csvParsingOptions['filename'] = $filename;
        $path = $this->csvParsingOptions['directory'].$this->csvParsingOptions['filename'];

        if ((false === file_exists($path)) || (false === fopen($path, 'r'))) {
            throw new PoiImportException('The given csv file doesn\'t exist : '.$filename);
        }

        $handle = fopen($path, 'r');

        while (false !== ($fileData = fgetcsv($handle, null, self::CSV_SEPARATOR))) {
            $poiData[] = $fileData;
        }

        if (empty($poiData)) {
            throw new PoiImportException('The given csv file '.$filename.' is empty ! There is no poi to import');
        }

        if ($this->csvParsingOptions['ignoreFirstLine']) {
            array_shift($poiData);
        }

        return $poiData;
    }

    private function processPoiAwaitingConfirmationImports(array $poiData, PoiImportEntity $poiImport): array
    {
        $awaitingPoiList = $this->createPoiAwaitingConfirmationList($poiData, $poiImport);
        $error = [];
        foreach ($awaitingPoiList as $awaitingPoi) {
            $searchingTerms = $this->buildSearchString($awaitingPoi);
            $error[] = $this->savePoiAwaitingConfirmation($searchingTerms, $awaitingPoi);
        }

        return $error;
    }

    private function updatePoiImportStatus(PoiImportEntity $poiImport, string $status)
    {
        $now = new \DateTime();
        $poiImport->setStatus($status)
        ->setUpdatedAt($now);
        $this->em->persist($poiImport);
        $this->em->flush();
    }

    private function savePoiAwaitingConfirmation(string $searchingTerms, PoiAwaitingConfirmation $awaitingPoi): string
    {
        $status = PoiAwaitingConfirmation::STATUS_NEW;

        try {
            $googlePoi = $this->poiFactory->getPoi($searchingTerms);
        } catch (FactoryException $e) {
            $awaitingPoi->setStatus(PoiAwaitingConfirmation::STATUS_ERROR);
            $this->em->persist($awaitingPoi);
            $this->em->flush();

            return 'awaitingPoiConfirmation id n°'. $awaitingPoi->getId(). ' error : '.$e->getMessage();
        }

        if (!empty($googlePoi->getId())) {
            $status = PoiAwaitingConfirmation::STATUS_EXISTING;
        }

        if (!empty($googlePoi->getName()) && !empty($googlePoi->getAddress())) {
            $this->em->persist($googlePoi);
            $this->em->flush();
            $awaitingPoi->setPoi($googlePoi)
            ->setStatus($status);
            $this->em->persist($awaitingPoi);
            $this->em->flush();
        } else {
            $awaitingPoi->setStatus(PoiAwaitingConfirmation::STATUS_ERROR);
            $this->em->persist($awaitingPoi);
            $this->em->flush();

            return 'awaitingPoiConfirmation id n°'. $awaitingPoi->getId(). ' error : Google Poi doesn\'t exist for poi_awaiting_confirmation id'.$awaitingPoi->getId().'.';
        }

        return '';
    }

    public function buildSearchString(PoiAwaitingConfirmation $awaitingPoi): string
    {
        if (empty($awaitingPoi->getName()) || empty($awaitingPoi->getCity())) {
            throw new PoiImportException('Cannot build searching string, there is no poi name or city set.');
        }

        $searchingString = '';
        $dataForSearch = $this->formatDataForSearchString($awaitingPoi);

        foreach ($dataForSearch as $data) {
            if (!empty($data)) {
                $searchingString .= $data.'+';
            }
        }

        return $searchingString;
    }

    private function formatDataForSearchString(PoiAwaitingConfirmation $awaitingPoi): array
    {
        $dataForSearch =
            [
                str_replace(' ', '+', $awaitingPoi->getName()),
                str_replace(' ', '+', $awaitingPoi->getAddress1()),
                str_replace(' ', '+', $awaitingPoi->getAddress2()),
                str_replace(' ', '+', $awaitingPoi->getPostalCode()),
                str_replace(' ', '+', $awaitingPoi->getCity()),
                str_replace(' ', '+', $awaitingPoi->getCountry()),
            ]
        ;

        return $dataForSearch;
    }

    private function createPoiAwaitingConfirmationList(array $poiData, PoiImportEntity $poiImport): array
    {
        $awaitingPoiList = [];

        foreach ($poiData as $poiKey => $poi) {
            $poiAwaitConfirmEntity = new PoiAwaitingConfirmation();
            try {
                $poiAwaitConfirmEntity
                    ->setName(trim($poi[0]))
                    ->setAddress1(trim($poi[1]))
                    ->setAddress2(trim($poi[2]))
                    ->setPostalCode(trim($poi[3]))
                    ->setCity(trim($poi[4]))
                    ->setCountry(trim($poi[5]))
                    ->setStatus(PoiAwaitingConfirmation::STATUS_NEW)
                    ->setPoiImport($poiImport);
                $this->em->persist($poiAwaitConfirmEntity);
                $this->em->flush();
                $awaitingPoiList[] = $poiAwaitConfirmEntity;
            } catch (\Exception $e) {
                $poiAwaitConfirmEntity->setStatus(PoiAwaitingConfirmation::STATUS_ERROR);
                $this->poiAwaitingConfirmationError[] = 'Error while creating poiAwaitingConfirmation : '. $e->getMessage();
            }
        }

        return $awaitingPoiList;
    }

    public function AutoCancelOutdatedPoiImport(): array
    {
        $report = [];
        $today = new \DateTime();
        $poiImportRepo = $this->em->getRepository(PoiImportEntity::class);
        $outdatedReadyPoiImportList = $poiImportRepo->findBy(['status' => PoiImportEntity::STATUS_READY]);

        if (!empty($outdatedReadyPoiImportList)) {
            foreach ($outdatedReadyPoiImportList as $poiImport) {
                $poiCreationDate = $poiImport->getCreatedAt();
                $expirationDate = new \DateTime($poiCreationDate->format('Y-m-d').'+ '.$poiImport::LIFETIME);

                if ($today > $expirationDate) {
                    $this->cleanOutdatedPoiAwaitingConfirmation($poiImport);
                    $poiImport->setStatus($poiImport::STATUS_CANCELED);
                    $poiImport->setUpdatedAt($today);
                    $this->em->flush();

                    $report[] = 'The poiImport ID '.$poiImport->getId(). ', created by '.$poiImport->getCreatedBy()->getLogin()
                        .' on '.$poiImport->getCreatedAt()->format('Y-m-d').' has been deleted.';
                }
            }
        }

        return $report;
    }

    private function cleanOutdatedPoiAwaitingConfirmation(PoiImportEntity $poiImport): void
    {
        $poiAwaitingConfirmationRepo = $this->em->getRepository(PoiAwaitingConfirmation::class);
        $poiAwaitingConfirmationList = $poiAwaitingConfirmationRepo->findAll();

        if (!empty($poiAwaitingConfirmationList)) {
            foreach ($poiAwaitingConfirmationList as $poiAwaitingConfirmation) {
                if ($poiAwaitingConfirmation->getPoiImport() === $poiImport) {
                    $this->em->remove($poiAwaitingConfirmation);
                    $this->em->flush();
                }
            }
        }
    }
}
