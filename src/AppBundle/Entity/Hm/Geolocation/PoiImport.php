<?php

namespace AppBundle\Entity\Db\Geolocation;

use Doctrine\ORM\Mapping as ORM;
use Path\To\User;

/**
 * @ORM\Table(name="poi_import")
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Db\Geolocation\PoiImportRepository")
 */
class PoiImport
{
    const STATUS_WAITING = 'WAITING';
    const STATUS_READY = 'READY';
    const STATUS_CANCELED = 'CANCELED';
    const LIFETIME = '30 days';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;

    /**
     * @ORM\Column(name="poi_total", type="integer")
     */
    private $poiTotal;

    /**
     * @ORM\Column(name="products_total", type="integer")
     */
    private $productsTotal;

    /**
     * @ORM\Column(name="products", type="json_array")
     */
    private $products = [];

    /**
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Path\To\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="util_id", nullable=false)
     * })
     */
    private $createdBy;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Path\To\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="canceled_by", referencedColumnName="util_id", nullable=false)
     * })
     */
    private $canceledBy;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPoiTotal(): int
    {
        return $this->poiTotal;
    }

    public function setPoiTotal(int $poiTotal): self
    {
        $this->poiTotal = $poiTotal;

        return $this;
    }

    public function getProductsTotal(): int
    {
        return $this->productsTotal;
    }

    public function setProductsTotal(int $productsTotal): self
    {
        $this->productsTotal = $productsTotal;

        return $this;
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function setProducts(array $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCanceledBy(): User
    {
        return $this->canceledBy;
    }

    public function setCanceledBy(User $canceledBy): self
    {
        $this->canceledBy = $canceledBy;

        return $this;
    }
}
