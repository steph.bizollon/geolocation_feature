<?php

namespace AppBundle\Entity\Db\Geolocation;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="poi_awaiting_confirmation")
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Db\Geolocation\PoiAwaitingConfirmationRepository")
 */

class PoiAwaitingConfirmation
{
    const STATUS_NEW = 'NEW';
    const STATUS_EXISTING = 'ALREADY_EXISTS';
    const STATUS_ERROR = 'ERROR';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="address1", type="string", length=255)
     */
    private $address1;

    /**
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(name="postal_code", type="string", length=50)
     */
    private $postalCode;

    /**
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(name="status", type="string", length=15)
     */
    private $status;

    /**
     * @var PoiImport
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Db\Geolocation\PoiImport", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="poi_import_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $poiImport;

    /**
     * @var Poi
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Db\Geolocation\Poi", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="poi_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $poi;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress1(): string
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPoiImport(): PoiImport
    {
        return $this->poiImport;
    }

    public function setPoiImport(PoiImport $poiImport): self
    {
        $this->poiImport = $poiImport;

        return $this;
    }

    public function getPoi(): ?Poi
    {
        return $this->poi;
    }

    public function setPoi(Poi $poi): self
    {
        $this->poi = $poi;

        return $this;
    }
}
