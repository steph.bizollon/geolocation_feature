<?php

namespace AppBundle\Repository\Db\Geolocation;

use AppBundle\Entity\Db\Geolocation\PoiAwaitingConfirmation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PoiAwaitingConfirmation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoiAwaitingConfirmation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoiAwaitingConfirmation[]    findAll()
 * @method PoiAwaitingConfirmation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoiAwaitingConfirmationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoiAwaitingConfirmation::class);
    }
}
