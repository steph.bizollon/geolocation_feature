<?php

namespace AppBundle\Repository\Db\Geolocation;

use AppBundle\Entity\Db\Geolocation\PoiImport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PoiImport|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoiImport|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoiImport[]    findAll()
 * @method PoiImport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoiImportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoiImport::class);
    }
}
